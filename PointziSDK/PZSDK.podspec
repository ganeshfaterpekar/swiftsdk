#
# Be sure to run `pod lib lint Umbrella.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#
#

Pod::Spec.new do |s|
s.name              = 'PZSDK'
s.version           = '1.0.0'
s.summary           = 'Description of Umbrella Framework.'

s.description      = <<-DESC
A bigger description of Umbrella Framework.
DESC

s.homepage          = 'https://pointzi.com'
s.license           = { :type => 'Apache-2.0', :file => 'LICENSE.txt' }
s.authors           = { 'Author one' => 'ganeshfaterpekar@gmail.com',
                         'Author two' => 'ganeshfaterpekar@gmail.com' }
s.source            = { :http => 'https://gitlab.com/ganeshfaterpekar/umbrellaframework/raw/master/Umbrella/PointziSDKFramework.zip'}

s.ios.deployment_target = '9.0'
s.ios.vendored_frameworks = 'PointziSDKFramework/PointziSDK.framework'

# Add all the dependencies
s.dependency 'Alamofire'

end
